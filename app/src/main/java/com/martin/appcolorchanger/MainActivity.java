package com.martin.appcolorchanger;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;

public class MainActivity extends AppCompatActivity {
    FloatingActionButton fab;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final Switch purpleSwitch = findViewById(R.id.purple_switch);
        final Switch pestoSwitch = findViewById(R.id.pesto_switch);
        final Switch orangeSwitch = findViewById(R.id.orange_switch);

        purpleSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            Resources r = getResources();

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    pestoSwitch.setChecked(false);
                    orangeSwitch.setChecked(false);
                    changeAppColor(r.getColor(R.color.colorPrimaryPurple), r.getColor(R.color.colorPrimaryDarkPurple), r.getColor(R.color.colorAccentPurple));
                } else {
                    changeAppColor(r.getColor(R.color.colorPrimary), r.getColor(R.color.colorPrimaryDark), r.getColor(R.color.colorAccent));
                }
            }
        });

        pestoSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            Resources r = getResources();

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    purpleSwitch.setChecked(false);
                    orangeSwitch.setChecked(false);
                    changeAppColor(r.getColor(R.color.colorPrimaryPesto), r.getColor(R.color.colorPrimaryDarkPesto), r.getColor(R.color.colorAccentPesto));
                } else {
                    changeAppColor(r.getColor(R.color.colorPrimary), r.getColor(R.color.colorPrimaryDark), r.getColor(R.color.colorAccent));
                }
            }
        });

        orangeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            Resources r = getResources();

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    purpleSwitch.setChecked(false);
                    pestoSwitch.setChecked(false);
                    changeAppColor(r.getColor(R.color.colorPrimaryOrange), r.getColor(R.color.colorPrimaryDarkOrange), r.getColor(R.color.colorAccentOrange));
                } else {
                    changeAppColor(r.getColor(R.color.colorPrimary), r.getColor(R.color.colorPrimaryDark), r.getColor(R.color.colorAccent));
                }
            }
        });
    }

    public void changeAppColor(int primaryColor, int primaryDark, int accentColor) {
        toolbar.setBackgroundColor(primaryColor);
        getWindow().setStatusBarColor(primaryDark);
        //НЕ находит этот цвет почему то, возмодно по другому метод или для каждого цвета свой метод сделать
        fab.setBackgroundTintList(ColorStateList.valueOf(accentColor));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
